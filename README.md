##### _Remark_
_This repository was forked from https://github.com/pmb6tz/windows-desktop-switcher and modified to fulfill my personnal needs. Some explanations come from the original Readme._   
   
   
   
# Windows Desktop Switcher
An AutoHotKey script for Windows that lets a user change virtual desktops by pressing CapsLock + &lt;num> or CapsLock + Left / Right arrow.

## Installation
Install AutoHotKey, then run the desktop_switcher.ahk script (open with AutoHotKey if prompted).

#### Standalone
The file desktop_switcher.exe is a standalone .exe that can be run on computers that don't have AutoHotkey installed. This file is compiled from the .ahk one.

## Auto-start at Login
Open "Run" (either press Windows Key + R, or search for it in the start menu) and type either (without quotes): "shell:startup" (to run the script for just the current user) or "shell:common startup" (to run it for all users). Paste it in the folder that opens.

## Hotkeys
        <CapsLock> + <Num> - Switches to virtual desktop "num"
        <CapsLock> + →     - Switch to virtual desktop on left
        <CapsLock> + ←     - Switch to virtual desktop on right

To change the key mappings, modify the bottom of the script and reload. Be sure to read about the [symbols AutoHotKey uses](https://autohotkey.com/docs/Hotkeys.htm) for key mapping.

## Other
To see debug messages, download [SysInternals DebugView](https://technet.microsoft.com/en-us/sysinternals/debugview).

Disclaimer: Tested only on Windows 10. Use at your own risk.